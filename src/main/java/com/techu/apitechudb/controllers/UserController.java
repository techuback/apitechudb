package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers (@RequestParam(name = "$orderby",defaultValue = "") String sortedBy) {
        System.out.println("UserController.getUsers");
        System.out.println("Ordenado: " + sortedBy);

        if (sortedBy.equals("name") || sortedBy.equals("age") || sortedBy.equals("id")) {
            return new ResponseEntity<>(this.userService.findAll(sortedBy), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(this.userService.findAll(""), HttpStatus.OK);
        }
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {
        System.out.println("UserController.getUserById");
        System.out.println("Id: " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "User not found",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel newUser) {
        System.out.println("UserController.addUser");
        System.out.println("UserId: " + newUser.getId());
        System.out.println("Name: " + newUser.getName());
        System.out.println("Age: " + newUser.getAge());

        return new ResponseEntity<>(this.userService.addUser(newUser),HttpStatus.OK);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUser(@RequestBody UserModel user,@PathVariable String id) {
        System.out.println("UserController.updateUser");
        System.out.println("Id: " + id);

        if (!user.getId().equals(id)) {
           return new ResponseEntity<>("Id missmatch, no update",HttpStatus.BAD_REQUEST);
        } else {
            Optional<UserModel> userToUpdate = this.userService.findById(id);
            if (userToUpdate.isPresent()) {
                System.out.println("User to update found!");
                this.userService.updateUser(user);
            }
            return new ResponseEntity<>(user,
                    userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND );
        }
    }
    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("UserController.deleteUser");
        System.out.println("Id a borrar: " + id);

        boolean result = this.userService.deleteUser(id);

        return new ResponseEntity<>(
                result ? "User Deleted!" : "User not found!",
                result ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
