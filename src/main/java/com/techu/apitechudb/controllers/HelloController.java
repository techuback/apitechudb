package com.techu.apitechudb.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @RequestMapping("/")
    public String index() {
        return "Hello from the other side.. ";
    }

    @RequestMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "sin nombre") String name) {
        return String.format("Hello %s from the other side...",name);
    }
}
