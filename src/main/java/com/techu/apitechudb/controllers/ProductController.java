package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("getProducts");

        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);
    }

    @GetMapping("products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id) {
        System.out.println("getProductsById");
        System.out.println("Id: " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "PRODUCT NOT FOUND",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel newProduct) {
        System.out.println("addProducts");
        System.out.println("Id: " + newProduct.getId());
        System.out.println("Desc: " + newProduct.getDesc());
        System.out.println("Price: " + newProduct.getPrice());

        return new ResponseEntity<>(this.productService.addProduct(newProduct),HttpStatus.OK);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("Id: " + product.getId());
        //TODO: Validar si el id coincide con el body
        Optional<ProductModel> productToUpdate = this.productService.findById(id);
        if (productToUpdate.isPresent()) {
            System.out.println("product to update found");

            product.setId(id);
            this.productService.update(product);
        }

        return new ResponseEntity<>(product,
                productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("Id Producto a borrar: " + id );

        boolean result = this.productService.delete(id);

        return new ResponseEntity<>(result ? "Product Deleted" : "Product not Found",result ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
}
