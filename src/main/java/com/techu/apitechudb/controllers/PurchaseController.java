package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.ProductService;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {
    @Autowired
    PurchaseService purchaseService;
    @Autowired
    UserService userService;
    @Autowired
    ProductService productService;

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases() {
        System.out.println(" PurchaseController.getPurchases");

        return  new ResponseEntity<>(this.purchaseService.findAll(),HttpStatus.OK);
    }

    @PostMapping("/purchases")
    public ResponseEntity<Object> addPurchase(@RequestBody PurchaseModel newPurchase) {
        System.out.println("PurchaseController.addPurchase");
        System.out.println("Id purchase: " + newPurchase.getId());
        System.out.println("Id user: " + newPurchase.getUserId());
        System.out.println("Items: " + newPurchase.getPurchaseItems());

        float purchaseAmount =  0;
        Optional<UserModel> user = userService.findById(newPurchase.getUserId());
        if (user.isEmpty()) {
            System.out.println("Usuario no encontrado:" + newPurchase.getUserId());
            return new ResponseEntity<>("UserId not found", HttpStatus.NOT_FOUND);
        } else {
            Optional<PurchaseModel> searchPurchase = this.purchaseService.findById(newPurchase.getId());
            if (searchPurchase.isPresent()) {
                return new ResponseEntity<>("Purchase ID already exists", HttpStatus.BAD_REQUEST);
            } else {
                for (Map.Entry<String,Integer> item : newPurchase.getPurchaseItems().entrySet()) {
                    Optional<ProductModel> searchProduct = this.productService.findById(item.getKey());
                    if (searchProduct.isEmpty()) {
                        return new ResponseEntity<>("Item id not found: " ,HttpStatus.BAD_REQUEST);
                    } else {
                        purchaseAmount += searchProduct.get().getPrice() * item.getValue();
                    }
                }
            }
        }
        newPurchase.setAmount(purchaseAmount);
        return new ResponseEntity<>(this.purchaseService.addPurchase(newPurchase),HttpStatus.CREATED);
    }
}
