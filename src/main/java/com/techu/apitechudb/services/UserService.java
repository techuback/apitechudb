package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(String sortedBy) {
        System.out.println("UserService.findAll");

        List<UserModel> result;
        if (sortedBy.equals("")) {
            result =  this.userRepository.findAll();
        } else {
            System.out.println("Returnig sorted list by " + sortedBy);
            result =  this.userRepository.findAll(Sort.by(sortedBy));
        }
        return result;
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("UserService.findById");

        return this.userRepository.findById(id);
    }

    public UserModel addUser(UserModel user) {
        System.out.println("UserService.addUser");

        return this.userRepository.save(user);
    }

    public UserModel updateUser(UserModel user) {
        System.out.println("UserService.updateUser");

        return this.userRepository.save(user);
    }

    public boolean deleteUser(String id) {
        System.out.println("UserService.deleteUser");

        boolean result = false;

        if (this.findById(id).isPresent()) {
            System.out.println("User to delete found");
            this.userRepository.deleteById(id);
            result = true;
        }
        return result;
    }

}
