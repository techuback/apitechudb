package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PurchaseService {
    @Autowired
    PurchaseRepository purchaseRepository;

    public PurchaseModel addPurchase(PurchaseModel purchase) {
        System.out.println("PurchaseService.addPurchase");

        return this.purchaseRepository.save(purchase);
    }

    public Optional<PurchaseModel> findById(String id) {
        System.out.println("PurchaseService.findById");

        return this.purchaseRepository.findById(id);
    }

    public List<PurchaseModel> findAll() {
        System.out.println("PurchaseService.findAll");

        return this.purchaseRepository.findAll();
    }
}
