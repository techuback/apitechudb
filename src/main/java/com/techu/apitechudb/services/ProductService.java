package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        System.out.println("ProductServices.findAll");
        return this.productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id) {
        System.out.println("ProductService.findById");

        return this.productRepository.findById(id);
    }

    public ProductModel addProduct(ProductModel product) {
        System.out.println("ProductService.addProduct");

        return this.productRepository.save(product);
    }

    public ProductModel update(ProductModel product) {
        System.out.println("ProductService.update");

        return this.productRepository.save(product);
    }

    public boolean delete(String id) {
        System.out.println("ProductService.delete");
        boolean result = false;

        if (this.findById(id).isPresent() == true) {
            System.out.println("Product to delete found");
            this.productRepository.deleteById(id);
            result = true;
        }
        return result;
    }


}
